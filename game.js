import World from './world.js'

HTMLCanvasElement.prototype.relativeCoords = function(event) {
    var x,y;
    var rect = this.getBoundingClientRect();
    x = event.clientX - rect.left;
    y = event.clientY - rect.top;
    var width = rect.right - rect.left;
    if(this.width!=width) {
        var height = rect.bottom - rect.top;
        x = x*(this.width/width);
        y = y*(this.width/height);
    }
    return [x,y];
};
const world = new World(400,300,10,10);
const  canv = document.getElementById("canvas");
var ctx = canv.getContext("2d");
ctx.beginPath();
ctx.strokeStyle  =  "blue";
ctx.rect(0,0, 400, 300);
ctx.stroke();
canv.addEventListener("click", function (event) {
    const coords = canv.relativeCoords(event);
   const rect =   world.getRect(coords[0],coords[1]);
    const element =    world.getElement(coords[0],coords[1]);
    var ctx = canv.getContext("2d");
    ctx.beginPath();
    ctx.strokeStyle = element==2?  "red":"blue";
    ctx.rect(rect[0], rect[1], rect[2], rect[3]);
    ctx.stroke();

});