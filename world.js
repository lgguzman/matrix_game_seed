export default class Word{

    constructor(width, height, tamX, tamY){
        this.width = width;
        this.height = height;
        this.tamX = tamX;
        this.tamY = tamY;
        this.weightX = width/tamX;
        this.weightY = height/tamY;
        this.matrix = new Array(tamY).fill(0).map(i=> {return new Array(tamX).fill(0)});
        this.matrix[0][1]=2;
        this.matrix[5][5]=2;
        console.log(this.matrix);

    }

    getIndexI (y){
            return Math.floor(y/ this.weightY)
    }
    getIndexJ(x){
        return Math.floor(x/ this.weightX)
    }
    getElement (x,y){
        return this.matrix[this.getIndexI(y)][this.getIndexJ(x)]
    }

    getRect (x,y){
        const i = this.getIndexI(y);
        const j = this.getIndexJ(x);
        return [ j* this.weightX, i* this.weightY , this.weightX, this.weightY ]
    }


}